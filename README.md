# Arduino Nerf

## Serial-Commands

Commands are single byte instructions in human readable ASCII form.
Baudrate: `38400`

| Command | Description |
|---------|-------------|
| `F` | Flywheel on 
| `f` | Flywheel off
| `n` | Rotate to next dart
| `P` | Push dart-pusher IN
| `p` | Push dart-pusher BACK
| `S` | Shoot (Macro for `Ppn` with correct delays)
| `0`-`8` | Flywheel speed (0=off)

| Response | Description |
|---------|-------------|
| `R` | on startup and after each exec. command |
| `?` | unknown command |
