/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_BUTTON
#define H_BUTTON

enum class ButtonState
{
    RELEASED, NEWPRESS, HELD
};

class Button
{
    private:
        ButtonState state = ButtonState::RELEASED;
        int analogPin;

    public:
        Button(int analogPin)
            : analogPin(analogPin)
        {}

        void update();
        bool isNewPress();
};

#endif
