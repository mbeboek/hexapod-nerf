/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#include <Arduino.h>
#include "StepperMotor.h"

void StepperMotor::turnRight()
{ // 1
  digitalWrite(pin3, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, LOW);
  delayMicroseconds(motorSpeed);

  // 2
  digitalWrite(pin3, HIGH);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, LOW);
  delayMicroseconds(motorSpeed);

  // 3
  digitalWrite(pin3, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, LOW);
  delayMicroseconds(motorSpeed);

  // 4
  digitalWrite(pin3, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin0, LOW);
  delayMicroseconds(motorSpeed);

  // 5
  digitalWrite(pin3, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin0, LOW);
  delayMicroseconds(motorSpeed);

  // 6
  digitalWrite(pin3, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin0, HIGH);
  delayMicroseconds(motorSpeed);

  // 7
  digitalWrite(pin3, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, HIGH);
  delayMicroseconds(motorSpeed);

  // 8
  digitalWrite(pin3, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, HIGH);
  delayMicroseconds(motorSpeed);
}

void StepperMotor::turnLeft()
{ // 1
  digitalWrite(pin0, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  delayMicroseconds(motorSpeed);

  // 2
  digitalWrite(pin0, HIGH);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  delayMicroseconds(motorSpeed);

  // 3
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
  delayMicroseconds(motorSpeed);

  // 4
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, HIGH);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin3, LOW);
  delayMicroseconds(motorSpeed);

  // 5
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin3, LOW);
  delayMicroseconds(motorSpeed);

  // 6
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, HIGH);
  digitalWrite(pin3, HIGH);
  delayMicroseconds(motorSpeed);

  // 7
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, HIGH);
  delayMicroseconds(motorSpeed);

  // 8
  digitalWrite(pin0, HIGH);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, HIGH);
  delayMicroseconds(motorSpeed);
}

void StepperMotor::stop()
{ digitalWrite(pin3, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin0, LOW);
}

void StepperMotor::turnRight(unsigned int steps) 
{
    for(unsigned int i=0; i<steps; ++i)
    {
        turnRight();
    }
    stop();
}

void StepperMotor::turnLeft(unsigned int steps) 
{
    for(unsigned int i=0; i<steps; ++i)
    {
        turnLeft();
    }
    stop();
}