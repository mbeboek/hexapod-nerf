/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_DC_MOTOR
#define H_DC_MOTOR

enum class DCMotorDirection
{
    STOPPED, FORWARDS, BACKWARDS
};

class DCMotor
{
    private:
        int pinSpeed;
        int pinForwards;
        int pinBackwards;

        u8 speed = 0;
        DCMotorDirection direction = DCMotorDirection::STOPPED;

    public:
        DCMotor(int pinSpeed, int pinForwards, int pinBackwards);

        void stop();
        void moveInDirection(DCMotorDirection direction);
        void setSpeed(u8 speed);
};

#endif