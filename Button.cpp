#include "Button.h"

#include <Arduino.h>

void Button::update()
{
    if(analogRead(analogPin) > 512)
    {
        if(state == ButtonState::RELEASED)
        {
            state = ButtonState::NEWPRESS;
        } else {
            state = ButtonState::HELD;
        }
        
    } else {
        state = ButtonState::RELEASED;
    }

}

bool Button::isNewPress()
{
    return state == ButtonState::NEWPRESS;
}