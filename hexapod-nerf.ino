#include "DcMotor.h"
#include "StepperMotor.h"
#include "Button.h"

#include <ServoTimer2.h>
//#include <Servo.h>
//#include <Adafruit_PWMServoDriver.h>

#define SERVO_POS_MIN 1000
#define SERVO_POS_MAX 2150

#define CMD_FLYWHEEL_ON  'F'
#define CMD_FLYWHEEL_OFF 'f'
#define CMD_NEXT_DART    'n'
#define CMD_PUSH_ON      'P'
#define CMD_PUSH_OFF     'p'
#define CMD_SHOOT        'S'
#define CMD_READY        'R' // this is sent out to indicate its ready for commands
#define CMD_UNKNOWN      '?'
#define CMD_SPEED_MIN    '0'
#define CMD_SPEED_MAX    '8'

class Main
{
  private:
    bool stepperEven = false;
    bool flywheelOn = false;

    DCMotor motorL = DCMotor(11, 9, 8);
    DCMotor motorR = DCMotor(10, 7, 6);
    StepperMotor dartWheel = StepperMotor(2, 3, 4, 5);

    Button btnFlywheel = Button(A6);
    Button btnShoot    = Button(A7);

    ServoTimer2 servoPush;

  public:

    int incPulse(int val, int inc){
      Serial.print(val + inc);
      Serial.print("\n");
        if( val + inc  > 3000 )
            return 1000 ;
        else
            return val + inc;  
      }

    void main()
    {
      Serial.begin(38400);

      servoPush.attach(13);
      servoPush.write(SERVO_POS_MIN);

      motorL.setSpeed(100);
      motorR.setSpeed(100);
      
      delay(100);
      Serial.write(CMD_READY);

      for(;;)
      {

        update();
        delay(10);
      }
    }

    inline void update()
    {
        btnFlywheel.update();
        btnShoot.update();  

        while (Serial.available()) 
        {
          executeCommand(Serial.read());
        }

        if(btnFlywheel.isNewPress()) {
          if(flywheelOn) {
            executeCommand(CMD_FLYWHEEL_OFF);
          } else {
            executeCommand(CMD_FLYWHEEL_ON);
          }
        }

        if(btnShoot.isNewPress()) {
          if(flywheelOn) {
            executeCommand(CMD_SHOOT);
          } else {
            executeCommand(CMD_NEXT_DART);
          }
        }
    }

    void executeCommand(char cmd)
    {
      switch(cmd)
      {
        case CMD_PUSH_OFF:
          servoPush.write(SERVO_POS_MIN);
        break;
        case CMD_PUSH_ON:
          servoPush.write(SERVO_POS_MAX);
        break;

        case CMD_FLYWHEEL_OFF:
          flywheelOn = false;
          motorL.stop();
          motorR.stop();
        break;
        case CMD_FLYWHEEL_ON:
          flywheelOn = true;
          motorL.moveInDirection(DCMotorDirection::FORWARDS);
          motorR.moveInDirection(DCMotorDirection::BACKWARDS);
        break;

        case CMD_NEXT_DART:
          dartWheel.turnRight(stepperEven ? 43 : 42);
          stepperEven = !stepperEven;
        break;

        case CMD_SHOOT:
          executeCommand(CMD_PUSH_ON);
          delay(500);
          executeCommand(CMD_PUSH_OFF);
          delay(600);
          executeCommand(CMD_NEXT_DART);
          delay(100);
        break;

        default:
          if(cmd >= CMD_SPEED_MIN && cmd <= CMD_SPEED_MAX) 
          {
            u8 motorSpeed = (u8)cmd - (u8)CMD_SPEED_MIN;
            motorSpeed = motorSpeed == 0 ? 0 : ((motorSpeed << 5) - 1);

            motorL.setSpeed(motorSpeed);
            motorR.setSpeed(motorSpeed);
          } 
          else 
          {
            Serial.write(CMD_UNKNOWN);
            return;
          }
        break;
      }

      Serial.write(CMD_READY);
    }
};

void setup() {   
  auto mainProg = Main();
  mainProg.main(); 
}
void loop(){}
