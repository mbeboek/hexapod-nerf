/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

#ifndef H_STEPPER_MOTOR
#define H_STEPPER_MOTOR

class StepperMotor
{
    private:
        const int pin0; // Blue   - In 1
        const int pin1; // Pink   - In 2
        const int pin2; // Yellow - In 3
        const int pin3; // Orange - In 4

        const int motorSpeed;

        void stop();
        void turnLeft();
        void turnRight();

    public:
        const static int DEFAULT_SPEED = 1000;

        StepperMotor(int pin0, int pin1, int pin2, int pin3)
            : pin0(pin0), pin1(pin1), pin2(pin2), pin3(pin3),
            motorSpeed(DEFAULT_SPEED)
        {
            pinMode(pin0, OUTPUT);
            pinMode(pin1, OUTPUT);
            pinMode(pin2, OUTPUT);
            pinMode(pin3, OUTPUT);

            stop();
        }
        
        void turnLeft(unsigned int steps);
        void turnRight(unsigned int steps);
};

#endif